import React from 'react';
import Tile from './Tile';

class Multiplication extends React.Component
{
	constructor(props)
	{
		super(props);
		const nbs = Array(2).fill(null);
		for(var i = 0; i < nbs.length; i++)
		{
			nbs[i] = Math.floor(Math.random() * 11);
		}

	    const answersTemp = Array(3).fill(null);
	    var correctIndex = Math.floor(Math.random() * 3);
	    answersTemp[correctIndex] = nbs[0] * nbs[1];
	    var usedRand = [answersTemp[correctIndex]];
	    for(var j = 0; j < answersTemp.length; j++)
	    {
	    	if(j != correctIndex)
	    	{

	    		var rand = Math.floor(Math.random() * 101);
	    		var equal = true;
	    		while (equal)
	    		{
	    			equal = false;
	    			rand = Math.floor(Math.random() * 101);
	    			for(var k = 0; k < usedRand.length; k++)
	    			{
	    				if(rand === usedRand[k])
	    					equal = true;
	    			}
				}
	    		answersTemp[j] = rand;
	    		usedRand.splice(0, 0, rand);
	    	}
	    }
	    console.log(usedRand);

		this.state = {
			addNbs: nbs,
			answers: answersTemp,
			answerMsg: "",
		};
	}

	checkAnswer(i)
	{
		if(i === (this.state.addNbs[0]*this.state.addNbs[1]))
		{

			this.setState({
				answerMsg: "Correct Answer",
			});
		}
		else
		{

			this.setState({
				answerMsg: "Wrong Answer",
			});
		}
	}

	nextQuestion()
	{
		const nbs = Array(2).fill(null);
		for(var i = 0; i < nbs.length; i++)
		{
			nbs[i] = Math.floor(Math.random() * 11);
		}

	    const answersTemp = Array(3).fill(null);
	    var correctIndex = Math.floor(Math.random() * 3);
	    answersTemp[correctIndex] = nbs[0] * nbs[1];
	    var usedRand = [answersTemp[correctIndex]];
	    console.log(usedRand);
	    for(var j = 0; j < answersTemp.length; j++)
	    {
	    	if(j !== correctIndex)
	    	{
	    		var rand = Math.floor(Math.random() * 101);
	    		var equal = true;
	    		while (equal)
	    		{
	    			equal = false;
	    			rand = Math.floor(Math.random() * 101);
	    			for(var k = 0; k < usedRand.length; k++)
	    			{
	    				if(rand === usedRand[k])
	    				{
	    					equal = true;
	    				}
	    			}
				}
	    		answersTemp[j] = rand;
	    		usedRand.splice(0, 0, rand);
	    		console.log("j : ", j);
	    		console.log("correctIndex : ", correctIndex);
	    		console.log(usedRand);
	    	}
	    }
	    console.log(usedRand);

		this.setState({
			addNbs: nbs,
			answers: answersTemp,
			answerMsg: "",
		});
	}

	renderTile(i)
	{
		return <Tile value={i} onClick={() => this.checkAnswer(i)}/>
	}

	render()
	{
		return (
			<div>
          		{this.renderTile(this.state.addNbs[0])}
          		*
          		{this.renderTile(this.state.addNbs[1])}
          		=?
          		{this.renderTile(this.state.answers[0])}
          		{this.renderTile(this.state.answers[1])}
          		{this.renderTile(this.state.answers[2])}
          		<div className="answer-text">{this.state.answerMsg}</div>
          		<div>
          			<button className="next-btn" onClick={() => this.nextQuestion()}>Next</button>
      			</div>
			</div>
			
		);	
	}
}
export default Multiplication;