import React from 'react';
import Addition from './Addition';
import Multiplication from './Multiplication';
import Subtraction from './Subtraction';

class Operation extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			operationChoice: "",
		}
	}

	chooseOperation(choice)
	{
		this.setState({operationChoice: choice,})
	}

	render()
	{
		if(this.state.operationChoice == "Addition")
		{
			return (
				<div>
					<Addition />
					<button onClick={() => this.chooseOperation("")}>Back</button>
				</div>
			);
		}
		else if(this.state.operationChoice == "Multiplication")
		{
			return (
				<div>
					<Multiplication />
					<button onClick={() => this.chooseOperation("")}>Back</button>
				</div>
			);
		}
		else if (this.state.operationChoice == "Subtraction")
		{
			return (
				<div>
					<Subtraction />
					<button onClick={() => this.chooseOperation("")}>Back</button>
				</div>
			);
		}
		else
		{
			return (
				<div>
					<button className="choice-btn" onClick={() => this.chooseOperation("Addition")}>Addition</button>
					<button className="choice-btn" onClick={() => this.chooseOperation("Subtraction")}>Subtraction</button>
					<button className="choice-btn" onClick={() => this.chooseOperation("Multiplication")}>Multiplication</button>
				</div>	
			);
		}
	}
}
export default Operation;