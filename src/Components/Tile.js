import React from 'react';

class Tile extends React.Component
{
	render()
	{
		return (
			<button className="tile" onClick={() => this.props.onClick()}>{this.props.value}</button>
		);	
	}
}
export default Tile;