import React from 'react';

import Operation from "./Components/Operation";
import logo from './logo.svg';

import './App.css';


function App() {
  return (
    <div className="App">
      <header className="App-header"> 
        <img src={logo} className="App-logo" alt="logo" />
        <p>Welcome to Brain Trainer</p>
        <Operation />
      </header>
    </div>
  );
}

export default App;
